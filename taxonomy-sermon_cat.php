<?php
/**
 * The template for displaying archive sermons.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Well_Church
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<div class="row">
				<div class="medium-10 medium-centered columns">
					<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</div>
			</div>
			<div class="sermon-filter-bg">
				<div class="row sermon-filter">
					<?php get_template_part('template-parts/sermon', 'filter'); ?>
				</div>
			</div>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					get_template_part( 'template-parts/content', 'sermons' );
				?>

			<?php endwhile; ?>

			<?php get_template_part('template-parts/sermon', 'pagination'); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
