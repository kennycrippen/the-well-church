<?php
/**
 * The template for displaying the front/home page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Well_Church
 */

get_header(); ?>

	<div class="img-hm-bleed">
		<span class="church"><?php the_post_thumbnail(); ?></span>
		<div class="join-us">
			<?php get_template_part( 'img/svg/svg', 'join-us' ); ?>
		</div>
		<div class="service-time-main">
			Sundays at 10:00 a.m.  •  1734 NE 1<sup>st</sup> Avenue  •  Portland, Or.
		</div>
	</div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'frontpage' ); ?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
