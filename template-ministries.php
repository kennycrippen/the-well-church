<?php
/**
 * The template for displaying the front/home page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Well_Church
 */
 /* ————————————————————————— */
 /* Template Name: Ministries
 /* ————————————————————————— */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
