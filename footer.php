<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Well_Church
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row">
			<div class="large-12 columns">
				<div class="site-info">
					<div class="well">
						<?php get_template_part( 'img/svg/well-only', 'logo' ); ?>
					</div>
					<div class="show-for-large-up">
						<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu' ) ); ?>
					</div>
					<div class="address">
						Sundays at 10:00 AM  •  1734 NE 1<sup>st</sup> Avenue, Portland, Oregon 97212
					</div>
					<a class="fb" href="https://www.facebook.com/The-Well-Community-Church-116488061713636">
						<?php get_template_part( 'img/svg/svg', 'icon-facebook' ); ?>
					</a>
				</div><!-- .site-info -->
			</div>
		</div>
		<div class="contact">
			<div class="row">
				<div class="medium-12 columns">
					<div class="-info">
						(503) 288-5502  •  info@thewellchurch.com  •  © 2015 The Well Community Church. All rights reserved.
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="https://npmcdn.com/flickity@2.0/dist/flickity.pkgd.js"></script>
</main>

<!-- the mobile nav -->
<?php get_template_part('inc/expandable', 'nav'); ?>

</body>
</html>
