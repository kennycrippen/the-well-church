<?php
/**
 * The template for displaying the front/home page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Well_Church
 */
 /* ————————————————————————— */
 /* Template Name: Giving
 /* ————————————————————————— */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<div class="row">
					<div class="medium-10 small-12 columns">
						<h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
					</div>
				</div>
			<?php endwhile; // End of the loop. ?>



    </main><!-- #main -->
  </div><!-- #primary -->
<?php get_footer(); ?>
