<?php
/**
 * The template for displaying the front/home page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Well_Church
 */
 /* ————————————————————————— */
 /* Template Name: Leadership
 /* ————————————————————————— */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<div class="row">
					<div class="medium-12 columns">
						<h1>Well <?php the_title(); ?></h1>
					</div>
				</div>
			<?php endwhile; // End of the loop. ?>

			<!-- begin repeater for elders -->
			<?php	if( have_rows('elders') ): ?>
				<div class="row">
					<div class="medium-12 columns">
						<h2>Elder Team</h2>
					</div>
				</div>
				<div class="row">
					<?php while ( have_rows('elders') ) : the_row();
					$image = get_sub_field('photo');
          $srcset = wp_get_attachment_image_srcset( $image['id'], 'full' );
					$name = get_sub_field('name');
					$contact = get_sub_field('phone_and_email');
					?>

			        <div class="medium-4 columns end">
                <img src="<?php echo $image['sizes']['full']; ?>" alt="<?php echo $image[alt]; ?>" srcset="<?php echo esc_attr( $srcset ); ?>" />
								<div class="name">
									<?php echo $name; ?>
								</div>
								<div class="contact">
									<?php echo $contact; ?>
								</div>
			        </div>

			    <?php endwhile; ?>
				</div>
			<?php endif; ?>

			<!-- begin repeater for staff -->
			<?php	if( have_rows('staff') ): ?>
				<div class="row">
					<div class="medium-12 columns">
						<h2>Church Staff</h2>
					</div>
				</div>
				<div class="row">
					<?php while ( have_rows('staff') ) : the_row();
					$image = get_sub_field('photo');
					$name = get_sub_field('name');
					$title = get_sub_field('title');
					$contact = get_sub_field('phone_and_email');
					?>

			        <div class="medium-4 columns end">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
								<div class="name">
									<?php echo $name; ?>
									<?php if($title) : ?>
										&nbsp;&nbsp;/&nbsp;&nbsp;<?php echo $title; ?>
									<?php endif; ?>
								</div>
								<div class="contact">
									<?php echo $contact; ?>
								</div>
			        </div>

			    <?php endwhile; ?>
				</div>
			<?php endif; ?>

			<!-- begin repeater for deacons -->
			<?php	if( have_rows('deacons') ): ?>
				<div class="row">
					<div class="medium-12 columns">
						<h2>Deacons</h2>
					</div>
				</div>
				<div class="row">
					<?php while ( have_rows('deacons') ) : the_row();
					$image = get_sub_field('photo');
					$name = get_sub_field('name');
					$title = get_sub_field('title');
					$contact = get_sub_field('phone_and_email');
					?>

			        <div class="medium-4 columns end">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
								<div class="name">
									<?php echo $name; ?>
									<?php if($title) : ?>
										&nbsp;&nbsp;/&nbsp;&nbsp;<?php echo $title; ?>
									<?php endif; ?>
								</div>
								<div class="contact">
									<?php echo $contact; ?>
								</div>
			        </div>

			    <?php endwhile; ?>
				</div>
			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
