<!-- filter by series -->
<div class="medium-5 medium-offset-1 columns">
	<label class="filter-label">Filter by Sermon Series:</label>
	<select id="FilterSelect" onChange="window.location.href=this.value">
		<option value="Series">Select a Sermon Series</option>
		<option value="<?php echo home_url(); ?>/sermons">All</option>
		<?php
		$terms = get_terms('sermon_cat', $args);
		$count = count($terms);
		$i=0;
		if ($count > 0) {
			foreach ($terms as $term) {
			$i++;
			$term_list  .= '<option value="'. home_url() . '/sermon-series/' . $term->slug .'">' . $term->name . '</option>';
				if ($count  != $i)
				{
					$term_list .= '';
				}
				else
				{
					$term_list .= '';
				}
			}
			echo $term_list;
		}
		?>

	</select>
</div>

<!-- filter by preacher -->
<div class="medium-5 medium-pull-1 columns">
	<label class="filter-label">Filter by Preacher:</label>
	<select id="FilterSelect" onChange="window.location.href=this.value">
		<option value="Series">Select a Preacher</option>
		<option value="<?php echo home_url(); ?>/sermons">All</option>
		<?php
		$termsp = get_terms('sermon_preacher', $args);
		$countp = count($termsp);
		$i=0;
		if ($countp > 0) {
			foreach ($termsp as $termp) {
			$i++;
			$term_listp  .= '<option value="'. home_url() . '/sermon-preacher/' . $termp->slug .'">' . $termp->name . '</option>';
				if ($count  != $i)
				{
					$term_listp .= '';
				}
				else
				{
					$term_listp .= '';
				}
			}
			echo $term_listp;
		}
		?>

	</select>
</div>
