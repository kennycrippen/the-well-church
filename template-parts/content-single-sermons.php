<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Well_Church
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<div class="row">
			<div class="medium-10 columns medium-centered">
				<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
			</div>
		</div>
	</div><!-- .entry-header -->

	<div class="entry-content">
		<div class="row">
			<div class="medium-10 medium-centered columns">
				<div class="sermon-meta">
					<?php global $post;
					$terms = get_the_terms($post->id, 'sermon_preacher');
					$termname = $terms[0]->name;
					$termlink = $terms[0]->slug ?>
					<div class="preacher">Preached by <a href="<?php echo home_url() . '/sermon-preacher/' . $termlink; ?>"><?php echo $termname; ?></a><span class="divider">&nbsp;/&nbsp;</span></div>
					<div class="date"><?php echo get_the_date(); ?><span class="divider">&nbsp;/&nbsp;</span></div>
					<div class="download"><a href="<?php echo get_the_content(); ?>">Download MP3</a></div>
				</div>
				<?php the_content(); ?>
				<?php if(get_field('verses')) : ?>
					<div class="verses">
						<h4>Sermon Text</h4>
						<?php echo get_field('verses'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div><!-- .entry-content -->


</article><!-- #post-## -->
