<?php
/**
 * Template part for displaying page content in front-page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Well_Church
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<!-- intro area -->
	<div class="entry-content-hm">
		<div class="row">
			<div class="large-10 columns large-centered">
				<?php the_content(); ?>
				<a href="/purpose/" class="button large">More About Us</a>
			</div>
		</div>
	</div>

	<!-- sermon call to action -->
	<div class="sermon-cta">
		<div class="row">
			<div class="medium-12 columns">
				<h2>Current Sermon Series</h2>
				<?php get_template_part( 'img/svg/svg', 'hebrews' ); ?>
				<a href="/sermon-series/hebrews/" class="button small">Listen Now</a>
			</div>
		</div>
	</div>

	<!-- 3d groups/events -->
	<div class="groups-events">
		<div class="row" data-equalizer>
			<div class="medium-10 medium-centered large-centered columns no-padding">
				<h2>Get Involved</h2>

				<!-- 3d groups -->
				<div class="medium-6 columns">
					<div class="groups-hm" data-equalizer-watch>
						<?php get_template_part( 'img/svg/icon', 'bible' ); ?>
						<h3>3D Community Groups</h3>
						<p class="about">
							Christ called us to love one another and be devoted to one another just as He loved us. At the Well, we desire to live out Christ-centered community that is authentic and holistic, encompassing every facet of our lives.
						</p>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>well-3-d-community-groups/">Join a Group »</a>
					</div>
				</div>

				<!-- events -->
				<?php
					$query = new WP_Query();
					$query->query(array(
					'post_type'                 => 'tribe_events',
					'posts_per_page'            => 1,
				));
				$post_count = $query->post_count;
				$count = 0;
				?>

				<div class="medium-6 columns">
					<div class="events-hm" data-equalizer-watch>
						<?php get_template_part( 'img/svg/icon', 'events' ); ?>
						<h3>Upcoming Events</h3>
						<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); $count++ ?>
							<h5 style="color: #ffffff; padding-left: 30px; margin-bottom: -5px;"><?php the_title(); ?></h5>
							<?php the_excerpt(); ?>
						<?php endwhile; ?>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>activities-the-well/">View All Events »</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- monthly flyer cta -->
	<?php if( have_rows('announcements_slider') ): ?>
		<div class="monthly-flyer entry-content-hm" style="background-image: url(<?php echo get_field('announcement_slider_background_image'); ?>); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="carousel" data-flickity='{ "autoPlay": 6000, "prevNextButtons": false }'>

				<?php while ( have_rows('announcements_slider') ) : the_row(); ?>
					<div class="row">
					<div class="medium-10 columns medium-centered">
						<div class="carousel-cell">
						<h2><?php echo get_sub_field('announcement_title'); ?></h2>
						<p><?php echo get_sub_field('announcement_sub_copy'); ?></p>
						<?php if(get_sub_field('button_text')) : ?>
							<a href="<?php echo get_sub_field('button_link'); ?>" class="button large"><?php echo get_sub_field('button_text'); ?></a>
						<?php endif; ?>
						</div>
					</div>
					</div>
				<?php endwhile; ?>

			</div>
		</div>
	<?php endif; ?>


</article><!-- #post-## -->
