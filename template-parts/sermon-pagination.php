<div class="row pagination">
  <div class="medium-10 columns medium-centered">
    <?php
    global $wp_query;

    $big = 999999999; // need an unlikely integer

    echo paginate_links( array(
      'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format' => '?paged=%#%',
      'current' => max( 1, get_query_var('paged') ),
      'prev_text'          => __('«'),
      'next_text'          => __('»'),
      'total' => $wp_query->max_num_pages
    ) );
    ?>
  </div>
</div>
