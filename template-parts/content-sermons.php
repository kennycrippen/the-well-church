<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Well_Church
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<div class="row">
			<div class="medium-10 columns medium-centered">
				<a href="<?php the_permalink(); ?>"><?php the_title( '<h3 class="entry-title">', '</h3>' ); ?></a>
			</div>
		</div>
	</div><!-- .entry-header -->

	<div class="entry-content">
		<div class="row">
			<div class="medium-10 medium-centered columns">
				<div class="sermon-meta">
					<?php global $post;
					$terms = get_the_terms($post->id, 'sermon_preacher');
					$termname = $terms[0]->name;
					$termlink = $terms[0]->slug ?>
					<div class="preacher">Preached by <a href="<?php echo home_url() . '/sermon-preacher/' . $termlink; ?>"><?php echo $termname; ?></a><span class="divider">&nbsp;/&nbsp;</span></div>
					<div class="date"><?php echo get_the_date(); ?><span class="divider">&nbsp;/&nbsp;</span></div>
					<div class="download"><a href="<?php echo get_the_content(); ?>">Download MP3</a><span class="divider">&nbsp;/&nbsp;</span></div>
					<div class="more"><a href="<?php the_permalink(); ?>">More Info</a></div>
				</div>
				<?php the_content(); ?>
			</div>
		</div>
	</div><!-- .entry-content -->


</article><!-- #post-## -->
