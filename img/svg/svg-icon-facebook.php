<svg version="1.1" id="svg-fb" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 54.8 55.3" style="enable-background:new 0 0 54.8 55.3;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#F7F3F2;}
</style>
<path class="st0" d="M27.5,0C12.4,0,0.2,12.2,0.2,27.3c0,15.1,12.2,27.3,27.3,27.3c15.1,0,27.3-12.2,27.3-27.3
	C54.8,12.2,42.6,0,27.5,0z M33.5,27.3h-3.8v13.9h-5.8V27.3H21v-4.8h2.9v-2.9c0-3.9,1.6-6.2,6.2-6.2H34v4.8h-2.4
	c-1.8,0-1.9,0.7-1.9,1.9l0,2.4H34L33.5,27.3z"/>
</svg>
