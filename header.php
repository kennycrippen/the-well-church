<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Well_Church
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Get the fonts -->
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab|Roboto:400,700' rel='stylesheet' type='text/css'>
<!-- Get the flickity css -->
<link rel="stylesheet" href="https://npmcdn.com/flickity@2.0/dist/flickity.css" media="screen">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header class="mobile-trigger is-fixed">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
		<?php get_template_part( 'img/svg/nowell', 'logo' ); ?>
	</a>
	<a id="cd-menu-trigger" href="#0"><span class="cd-menu-icon"></span></a>
</header>
<main class="cd-main-content">
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'the-well-church' ); ?></a>

	<header id="masthead" class="site-header top" role="banner">
		<div class="row">
			<div class="large-12 columns">
				<div class="site-branding">
					<a class="header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<?php get_template_part( 'img/svg/well', 'logo' ); ?>
					</a>
				</div><!-- .site-branding -->
				<div class="nav-holder">
					<nav id="site-navigation" class="main-navigation" role="navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
					</nav><!-- #site-navigation -->
					<div class="phone-social">
						<span class="header-phone">(503) 288-5502</span>
						<a href="https://www.facebook.com/The-Well-Community-Church-116488061713636">
							<?php get_template_part( 'img/svg/svg', 'icon-facebook' ); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
