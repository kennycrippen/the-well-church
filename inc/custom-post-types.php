<?php

// let's create the function for the custom type
function custom_post_podcast() {
	// creating (registering) the custom type
	register_post_type( 'sermons', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Sermons', 'the-well-church'), /* This is the Title of the Group */
			'singular_name' => __('Sermon', 'the-well-church'), /* This is the individual type */
			'all_items' => __('All Sermons', 'the-well-church'), /* the all items menu item */
			'add_new' => __('Add New', 'the-well-church'), /* The add new menu item */
			'add_new_item' => __('Add New Sermon', 'the-well-church'), /* Add New Display Title */
			'edit' => __( 'Edit', 'the-well-church' ), /* Edit Dialog */
			'edit_item' => __('Edit Sermons', 'the-well-church'), /* Edit Display Title */
			'new_item' => __('New Sermon', 'the-well-church'), /* New Display Title */
			'view_item' => __('View Sermon', 'the-well-church'), /* View Display Title */
			'search_items' => __('Search', 'the-well-church'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'the-well-church'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'the-well-church'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the sermons custom post type', 'the-well-church' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			// 'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			// 'rewrite'	=> array( 'slug' => 'sermons', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type('category', 'sermon_cat');

}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_podcast');

	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/

	// now let's add custom categories (these act like categories)
    register_taxonomy( 'sermon_cat',
    	array('sermons'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */
    		'labels' => array(
    			'name' => __( 'Sermon Series', 'the-well-church' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Sermon Series', 'the-well-church' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Sermon Series', 'the-well-church' ), /* search title for taxomony */
    			'all_items' => __( 'All Sermon Series', 'the-well-church' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Category', 'the-well-church' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Category:', 'the-well-church' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Sermon Series', 'the-well-church' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Sermon Series', 'the-well-church' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Sermon Series', 'the-well-church' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Category Name', 'the-well-church' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'sermon-series' ),
    	)
    );

		register_taxonomy( 'sermon_preacher',
    	array('sermons'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */
    		'labels' => array(
    			'name' => __( 'Sermon Preacher', 'the-well-church' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Preacher', 'the-well-church' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Preachers', 'the-well-church' ), /* search title for taxomony */
    			'all_items' => __( 'All Preachers', 'the-well-church' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Category', 'the-well-church' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Category:', 'the-well-church' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Preacher', 'the-well-church' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Preacher', 'the-well-church' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Preacher', 'the-well-church' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Preacher Name', 'the-well-church' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'sermon-preacher' ),
    	)
    );



?>
